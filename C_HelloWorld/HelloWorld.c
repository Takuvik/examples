/* ================================================================== 
  HelloWorld.c
  
  Created by Maxime Benoit-Gagne on 2017-05-19.
  Takuvik - Canada.
  
  compiler:
  $ gcc -v
  Configured with: --prefix=/Applications/Xcode.app/Contents/Developer/usr --with-gxx-include-dir=/usr/include/c++/4.2.1
  Apple LLVM version 5.1 (clang-503.0.40) (based on LLVM 3.4svn)
  Target: x86_64-apple-darwin12.6.0
  Thread model: posix
  
  usage:
  ./HelloWorld
  
  description: Output "Hello World!".
  
  uses: 
  
  keywords: C, HelloWorld
  ================================================================== */

#include <stdio.h>
#include <stdlib.h>

/* ================================= MAIN ================================= */

int main(int argc, char *argv[]){
  
  char errmsg[] = "Bad number of arguments.\n"
  "Usage:\n"
  "./HelloWorld\n";
  
  /////////// Declaration of the variables. ///////////
  
  /////////// Verification of the arguments. ///////////
  if(argc != 1){
    printf("%s", errmsg);
    return -1;
  }
  
  /////////// HelloWorld ///////////

  printf("Hello World!\n");
  
  /////////// Return. ///////////
  return 0;
}
