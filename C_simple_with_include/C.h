/* ================================================================== 
  C.h
  
  Created by Maxime Benoit-Gagne on 2017-05-19.
  Takuvik - Canada.
  
  compiler:
  $ gcc -v
  Configured with: --prefix=/Applications/Xcode.app/Contents/Developer/usr --with-gxx-include-dir=/usr/include/c++/4.2.1
  Apple LLVM version 5.1 (clang-503.0.40) (based on LLVM 3.4svn)
  Target: x86_64-apple-darwin12.6.0
  Thread model: posix
  
  usage:
  To be used by another C++ code.
  
  description: contains one function
  
  uses: 
  
  keywords: example
  ================================================================== */

/* ================================= MAIN ================================= */

/*
 * @return 1.
 */
int get1();
