/* ================================================================== 
  test_include.c
  
  Created by Maxime Benoit-Gagne on 2017-05-19.
  Takuvik - Canada.
  
  compiler:
  $ gcc -v
  Configured with: --prefix=/Applications/Xcode.app/Contents/Developer/usr --with-gxx-include-dir=/usr/include/c++/4.2.1
  Apple LLVM version 5.1 (clang-503.0.40) (based on LLVM 3.4svn)
  Target: x86_64-apple-darwin12.6.0
  Thread model: posix
  
  usage:
  ./test_include
  
  description: print "Number: 1"
  
  uses: 
  
  keywords: example
  ================================================================== */

#include <stdio.h>
#include <stdlib.h>

#include "C.h"

/* ================================= MAIN ================================= */

int main(int argc, char *argv[]){
  
  char errmsg[] = "Bad number of arguments.\n"
  "Usage:\n"
  "./test_include.c\n";
  int number;
  
  /////////// Declaration of the variables. ///////////
  
  /////////// Verification of the arguments. ///////////
  if(argc != 1){
    printf("%s", errmsg);
    return -1;
  }
  
  /////////// TestInclude ///////////

  number = get1();
  printf("Number: %d\n", number);
  
  /////////// Return. ///////////
  return 0;
}
