#!/bin/bash

# file: grdview_example.sh
# description: example of grdview in GMT4.
#              produce a map of the chlorophyll-a GSM.

# version 1
# Maxime Benoit-Gagne
# July 11, 2014

# Documentation on gmt functions can be found at
# http://gmt.soest.hawaii.edu/gmt4/gmt/html/gmt_man.html

#=============================================================================
#   constants and parameters
#=============================================================================

           ############################################
           #                TO MODIFIY                #
           ############################################

# Name of the chl_gsm.
# No extension in the name.
# The format is GMT text with no header.
chl_gsm_file="SM2007225_chl_gsm"

region=-180/180/45/90

           ############################################
           #                END OF TO MODIFIY         #
           ############################################

proj=S-90/90/16c

#=============================================================================
#   main program
#=============================================================================

gmtset \
    COLOR_FOREGROUND darkred \
    COLOR_BACKGROUND white \
    COLOR_NAN pink

###################### chl_gsm ######################
# makecpt: Make GMT color palette tables.
# Create a color palette (.cpt) file.
table=rainbow
cpt_range=.01/10/2

makecpt -C${table} -Qo -T${cpt_range} -V -Z \
    > scale_chl_gsm.cpt

# xyz2grd: Converting an ASCII or binary table to grid file format.
# Create a grid (.grd) file.
xyzfile=${chl_gsm_file}.gmt
grdfile=${chl_gsm_file}.grd
inc=9.28k

xyz2grd ${xyzfile} -G${grdfile} -I${inc} -R${region} -F -Hi -V

# grdview: Create 3-D perspective grayshaded/colored image or mesh from a 2-D 
# grid file.
# Create a PostScript (ps) file.
relief_file=${chl_gsm_file}.grd
scale=scale_chl_gsm.cpt
psfile=${chl_gsm_file}.ps

grdview ${relief_file} -J${proj} -C${scale} -K -R${region} -Ts -V \
    > ${psfile}

scale=scale_chl_gsm.cpt
psfile=${chl_gsm_file}.ps

# pscoast: To plot land-masses, water-masses, coastlines, borders, and rivers.
# Append to the PostScript (ps) file.
boundary_tickmarks=30g30/a10g10

pscoast -J${proj} -R${region} -B${boundary_tickmarks} -Df -G40 -K -O \
    >> ${psfile}

# pstext: To plot text strings on maps.
# Append to the PostScript (ps) file.
region_pstext=0/16/0/16
proj_pstext=X16/16
title="SeaWiFS and MODIS-Atmosphere"

echo "4.5 17.5 14. 0 3 LM ${title}" | \
    pstext -J${proj_pstext} -R${region_pstext} -K -N -O -V \
    >> ${psfile}

pstext -J${proj} -R${region} -K -O -V <<EOF>> ${psfile}
-180 50 12 -90 0 MC 50@+\260@+
-180 60 12 -90 0 MC 60@+\260@+
-180 70 12 -90 0 MC 70@+\260@+
-180 80 12 -90 0 MC 80@+\260@+
EOF

year=2006
day=225
year_day=${year}-${day}

echo "15.5 .0 14 0 3 LM ${year_day}" | \
    pstext -J${proj_pstext} -R${region_pstext} -K -N -O -V \
    >> ${psfile}

# psscale: Plot gray scale or color scale on maps.
# Append to the PostScript (ps) file.
colorbar_par=":chl_gsm:/:mgChla.m@+-3@+:"
pos=17c/8c/10c/.35c

psscale -D${pos} -B${colorbar_par} -C${scale} -E -L -O -V \
    >> ${psfile}

# ps2raster: Converts one or several PostScript file(s) to other formats using 
# GhostScript.
# Create a JPEG (jpg) file.
ps2raster ${psfile} -A -P -Tj -V
