# Exemple of rsync command

<br>

`rsync -avr --progress /Users/ /Volumes`

<br>

Source : /Users/

Destination : /Volumes

<br>

a = archive - means it preserves permissions (owners, groups), times symbolic links, and devices.

r = recursive - means it copies directories and sub directories

v = verbose - means that it prints on the screen what is being copied


